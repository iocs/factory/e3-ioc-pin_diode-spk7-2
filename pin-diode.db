record(calc, "$(P)$(R_LTC2991)-CurrMonitor") {
    field(DESC, "Current Consumption")
    field(EGU,  "mA")
    field(PREC, "3")
    field(CALC, "(A-B)*2")
    field(INPA, "$(P)$(R_LTC2991)-ValueV1_RBV CP")
    field(INPB, "$(P)$(R_LTC2991)-ValueV2_RBV CP")

    info(DESCRIPTION, "Current consumption")
    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_LTC2991)-ValueV5_RBV") {
    alias("$(P)$(R_LTC2991)-5vDigVol")

    field(DESC, "Digital 5V Voltage")
    field(EGU,  "V")
    field(ASLO, "2.0")

    info(autosaveFields, "ASLO")
    info(DESCRIPTION, "Digital 5V voltage")
    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_LTC2991)-ValueV6_RBV") {
    alias("$(P)$(R_LTC2991)-AnaVol")

    field(DESC, "Analog Voltage")
    field(EGU,  "V")
    field(ASLO, "2.0")

    info(autosaveFields, "ASLO")
    info(DESCRIPTION, "Analog voltage")
    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_LTC2991)-Trigger") {
    field(SCAN, ".5 second")

    info(autosaveFields, "SCAN")
}

record("*", "$(P)$(R_LTC2991)-Read") {
    field(SCAN, ".5 second")

    info(autosaveFields, "SCAN")
}

record("*", "$(P)$(R_TMP100)-Read") {
    field(SCAN, ".5 second")

    info(autosaveFields, "SCAN")
}

record("*", "$(P)$(R_TMP100)-Value_RBV") {
    field(EGU,  "C")

    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_TCA9555)-Read") {
    field(SCAN, ".5 second")

    info(autosaveFields, "SCAN")
}

record(bi, "$(P):SwitchStatus") {
    field(DESC, "Pin diode switch status")
    field(INP,  "$(P)$(R_TCA9555)-LevelPin7_RBV CP")
    field(ONAM, "Closed")
    field(ZNAM, "Open")

    info(DESCRIPTION, "Pin diode switch status")
    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_TCA9555)-LevelPin0") {
    alias("$(P)$(R_TCA9555)-TestLED1")

    field(DESC, "Test LED1 Control")

    info(DESCRIPTION, "Test LED 1")
    info(autosaveFields, "VAL")
}

record("*", "$(P)$(R_TCA9555)-LevelPin0_RBV") {
    alias("$(P)$(R_TCA9555)-TestLED1_RBV")

    field(DESC, "Test LED1 Status")

    info(DESCRIPTION, "Test LED 1 status")
    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_TCA9555)-LevelPin1") {
    alias("$(P)$(R_TCA9555)-TestLED2")

    field(DESC, "Test LED2 Control")

    info(DESCRIPTION, "Test LED 2")
    info(autosaveFields, "VAL")
}

record("*", "$(P)$(R_TCA9555)-LevelPin1_RBV") {
    alias("$(P)$(R_TCA9555)-TestLED2_RBV")

    field(DESC, "Test LED2 Status")

    info(DESCRIPTION, "Test LED 2 status")
    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_TCA9555)-DirPin0") {
    alias("$(P)$(R_TCA9555)-DirTestLED1")

    info(ARCHIVE_THIS, "")
}

record("*", "$(P)$(R_TCA9555)-DirPin1") {
    alias("$(P)$(R_TCA9555)-DirTestLED2")

    info(ARCHIVE_THIS, "")
}
